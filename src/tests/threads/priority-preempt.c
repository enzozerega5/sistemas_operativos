/* Ensures that a high-priority thread really preempts.

   Based on a test originally submitted for Stanford's CS 140 in
   winter 1999 by by Matt Franklin
   <startled@leland.stanford.edu>, Greg Hutchins
   <gmh@leland.stanford.edu>, Yu Ping Hu <yph@cs.stanford.edu>.
   Modified by arens. */

#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/init.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"

static thread_func simple_thread_func;

void
test_priority_preempt (void) 
{
  /* This test does not work with the MLFQS. */
  ASSERT (selected_scheduler != SCH_MLFQS);

  /* Make sure our priority is the default. */
  //ASSERT (thread_get_priority () == PRI_DEFAULT);

  thread_create ("63", 63, simple_thread_func, NULL);
  thread_create ("61", 63, simple_thread_func, NULL);
  thread_create ("2", 3, simple_thread_func, NULL);
  thread_create ("8", 8, simple_thread_func, NULL);
  thread_create ("45", 45, simple_thread_func, NULL);
  thread_create ("10", 10, simple_thread_func, NULL);
  thread_create ("58", 58, simple_thread_func, NULL);
  thread_create ("18", 18, simple_thread_func, NULL);
  thread_create ("25", 25, simple_thread_func, NULL);
  thread_create ("15", 15, simple_thread_func, NULL);
  thread_create ("30", 30, simple_thread_func, NULL);
  thread_create ("63", 63, simple_thread_func, NULL);
  thread_create ("41", 41, simple_thread_func, NULL);
  thread_create ("1", 1, simple_thread_func, NULL);

  msg ("The high-priority thread should have already completed.");
}

static void 
simple_thread_func (void *aux UNUSED) 
{
  int i;
  
  for (i = 0; i < 10; i++) 
    {
      msg ("Thread %s iteration %d", thread_name (), i);

      msg("La prioridad del thread actual es %d", thread_get_priority());
      thread_yield ();
    }
  msg ("Thread %s done!", thread_name ());
    
    /*
printf("\nReporte de finalizacion del Thread %s:", thread_name());
printf("\nTiempo que el thread estuvo ejecutando: %lf", thread_ticksSeleccionado()/100);
printf("\nTiempo que el thread estuvo bloqueado: %d", thread_ticksBloqueado());
printf("\nCantidad de veces que el thread fue elegido para ejecutar: %d",thread_seleccionado ());
printf("\nCantidad de veces que el thread pasa estado bloqueado: %d ", thread_bloqueado ());
printf("\nPrioridad del thread: %d \n", thread_get_priority());
*/

 //   msg ("La prioridad es %d", thread_get_priority());
}
